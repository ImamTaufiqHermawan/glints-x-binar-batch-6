# This is Code Challenge 01
---
## Expaination

```javascript
const readline = require('readline');
```

The Readline module provides a way of reading a datastream, one line at a time.

```javascript
  input: process.stdin,
  output: process.stdout
```

process.stdin means "input coming from the terminal"

process.stdout means for output, use the standard output stream (i.e. terminal console output)

we can say these code above to get input from a readable stream such as the process.stdin stream, which during the execution of a Node.js program is the terminal input, one line at a time.

```javascript
const currentDate = new Date(); 
```

To get current date

```javascript
const age = currentDate.getFullYear() - birthday; 
```
To get current year from currentDate then minus it with year birthday voila we get age.