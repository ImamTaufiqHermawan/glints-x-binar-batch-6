# This is Code Challenge 01
-----------------
we have learned a little bit about Javascript. Now, we get challenge to sharpen our knowledge about what we have learn in this week!
 

## The Challenge 
----------------------
We must create greet() function declaration with source code that our mentor Mr. Fikri already prepared it for us to finish the code. So that function will return a string and log it to the terminal, and it should be like this:

```javascript
Hello, Fikri! Looks like you're 20 years old, and you lived in Surakarta!
```

Once we have done with it, we submit our task in our repository GlintsXBinar.

----
## Blocker
-----
I don't have any blocker with this challenge, but it is quite challenging especially with us must print user age but user only input his/her birthday year. so we must find code that count current year with user birthday year. 