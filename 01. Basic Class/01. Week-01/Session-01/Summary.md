# Session 01

# What have I learnt?
* I learnt about Backend.
* I learnt Backend Engineer job description.
* I learnt about Node.js.
* Basic Linux Commands for Beginners such as : 
    1) pwd      - command to know which directory we are in.
    2) ls       - command to know what files are in the directory we are in. 
    3) cd       - command to go to a directory. 
    4) cd ..    - command to go back from a folder that we are in.
    5) mkdir    - command to create a folder or a directory.
    6) rm       - command to delete a file or directory.
    7) touch    - command to create a file.
    8) cp       - command to copy files through the command line. 
    9) cat      - command to display the contents of a file.
    10) whoami  - command to see user.
* I managed to create a hello world application using Node.JS