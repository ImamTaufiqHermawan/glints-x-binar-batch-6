# Session 03

# What have I learnt?
* I learnt about Git, Git Service and Git Commands such as : 
    1) <b>git init</b> - to create a new git repository
    2) <b>git add</b>  - propose changes
    3) <b>git commit</b> -  commit changes
    4) <b>git push origin master</b>   - to upload/send changes to remote repository
    5) <b>git branch (add)</b> - create new branch 
    6) <b>git checkout</b> - to switch which branch we want
    7) <b>git pull</b>   - to download from repo to local 
    8) <b>git status</b> - to see what change
    9) <b>git clone</b> - copy of a local repository
* I created my local repository then connect and upload it to my Gitlab. 