# Session 05

## What have I learnt?
I Learnt much more javascript commands :

### different between let, const, var 
<p>it can be declaration etc.</p>

<b><u>let</b></u> 

* Variables that change, but we cannot re-declare the name of that variable
* block scoped
* ReferenceError when accessing a variable before it's declared
* can be reassigned

<b><u>const</b></u> 

* immutable Variable
* block scoped
* ReferenceError when accessing a variable before it's declared
* can't be reassigned

<b><u>var</b></u>

*  function scoped
* undefined when accessing a variable before it's declared

<b><u>function </b></u>

I learn more function command.

function signature
* function function.name(){
    
    // Run the code here

}

function.name(); // to call the function

<u>example :</u>
``` javascript
function test(){
    var hasil = x - 2;
    console.log(hasil);
}

test();
```

I improved my code in group project from previous session (session 04). [This is my group project](https://gitlab.com/Nattooo/session-4-team-a)

I learnt about Primitive data type such as :
* ***Number*** = 10, 123, etc.
* ***integer*** = 10, 20, 30, etc.
* ***Bigint*** =  109208401248912479012470724091
* ***float*** = 3.14, 5.16, etc
* ***double*** = 3.141241241245
* ***String*** = data type used in programming, such as an integer and floating point unit, but is used to represent text rather than numbers. It is comprised of a set of characters that can also contain spaces and numbers. For example, the word "children".
* ***Boolean*** = expressions use the operators AND, OR, XOR, and NOT to compare values and return a true or false result. These boolean operators are described in the following four examples: x AND y - returns True if both x and y are true; returns False if either x or y are false.
* ***Null*** = It is used to signify missing or unknown values. The keyword NULL is used to indicate these values. NULL really isn't a specific value as much as it is an indicator.

* ***Undefined*** = any object that has been declared but not initialized or defined. In other words, in a case where no value has been explicitly assigned to the variable, JavaScript calls it 'undefined'. ... The return value of functions that have to but don't return a value.
* ***Object*** = In JavaScript, an object is a standalone entity, with properties and type. Compare it with a cup, for example. A cup is an object, with properties. A cup has a color, a design, weight, a material it is made of, etc.
* ***Array*** = single variable that is used to store different elements. It is often used when we want to store list of elements and access them by a single variable.
* ***Set*** = a collection of items which are unique i.e no element can be repeated. Set in ES6 are ordered: elements of the set can be iterated in the insertion order. Set can store any types of values whether primitive or objects
* ***Symbol*** = used to identify object properties. Often to avoid name clashing between properties, since no symbol is equal to another.