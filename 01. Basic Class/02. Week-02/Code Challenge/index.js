const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
  // Code Here
  data = clean(data);
  let swapped;
  do {
    swapped = false;
    for (let i = 0; i < data.length; i++) {
      if (data[i] > data[i + 1]) {
        let tmp = data[i];
        data[i] = data[i + 1];
        data[i + 1] = tmp;
        swapped = true;
      }
    }
  } while (swapped);
  return data;
};

// Should return array
function sortDecending(data) {
  // Code Here
  data = clean(data);
  let swapped
  do {
    swapped = false;
    for (let i = 0; i < data.length; i++) {
      if (data[i] < data[i + 1]) {
        let tmp = data[i];
        data[i] = data[i + 1];
        data[i + 1] = tmp;
        swapped = true;
      }
    }
  } while (swapped);
  return data;
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
