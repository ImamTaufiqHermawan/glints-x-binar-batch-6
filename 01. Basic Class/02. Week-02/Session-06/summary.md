# Session-06

## Array

### Method

Every data have their own behavior, for example string:
```javascript
method.lenght //this is use fore measure how many characte is the string
```
### Looping in Array

There are several ways:

* for
* for each
* for of

```js
// Using for
// i = 0 -> initial value
// i < 10 -> break statement
// i++ -> incremental
for(let i =0; i < 10; i++){
    // Do something here
    console.log('Hello World');
}
```

### Data type
how do we know what data is the function or variable or value?
use `typeof` command, typeof have 2 function, such as a operator & as aFunction itself
```javascript
console.log(typeof <nameofvariable>);
```

### Looping in Object
```js
// Using for in
const Student = {
    name: 'rijal',
    age: 25
}
for (s in Student){
    console.log(s);
    /*
    It will print its keys, like this:
    name
    age
    */
}
```

## If else Statement
is a function that makes a statement will produce a value according to some estimated situation

if else statement will only read a `bolean` data
```java script
let raining = true;//variable Global
let midnight = true;//variable Global

if (raining && !midnight) {
  console.log("Pake payung");
} else if (raining && midnight) {
  console.log("Pesen Gocar");
} else {
  console.log("Gak usah pake payung");
};
//according to the variable, this function will produce "pesen gocar" value.
```

### Object
object is a list of value that we can give a name for each value, we usually called it `key value`
```js
let numbers = [1,2,3,4,5]
console.log(numbers.join(","));

let obj = {
  name: "Fikri Rahmat Nurhidayat",
  isMarried: false,
  address: {
    city: "Solo",
    province: "Jawa Tengah"
  }
}
```

### Logical Operator

* && (AND)

Returns true if both operands are true/false 

* || (OR)

If any of its arguments are true, it returns true, otherwise it returns false.

```js
let a = 10;
let b = 15;
if (a > 5 && b > 5){
    // Because, these 2 conditionals are true, so this following code will run
    console.log('This is an AND Operator');
}else{

}
if (a > 5 || b < 10) {
    // Because one of 1 arguments is true, then it will return true
    console.log('This is an OR Operator');
}
```

### Validation
function to validate what data we want to be execute

```javascript
function register(email, password) {
    if (typeof email === 'string' && typeof password === 'string') {
    console.log('Registering....');
      } else {
        throw new Error("Input is not valid!");
      };
    }
    register('string1', 'string2');
    
    function register(email, password) {
    if (typeof email !== 'string' && typeof password !== 'string') {
        throw new Error("Input is not valid!");
        };

    console.log('Registering....');
    }
```

