const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

console.clear(); 
console.log("Which one do you want to calculate?");
console.log(`
1. Square Area
2. Square Around
`)

function CalcsquareArea(side) {
        return side * side;
}

function CalcsquareAround(side){
        return 4 * side;
}

function handleAnswer1() {
  console.clear();
  console.log("Calculate Square Area");

  rl.question("Side: ", side => {
    
       console.log(
         "Here's the result:",
         CalcsquareArea(side)
       )

       rl.close();
    })
}
  
  function handleAnswer2() {
    console.clear();
    console.log("Calculate Square Around");
  
    rl.question("Side: ", side => {
      if (+side == 'number') {
        console.log("nomor oy");
      } else {
         console.log(
           "Here's the result:",
           CalcsquareAround(side)
         )
  
         rl.close();
         }
        })
  }
  
  rl.question('Answer: ', answer => {
    if (answer == 1) {
      handleAnswer1()
    }
  
    else if (answer == 2) {
      handleAnswer2()
    }
  
    else {
      console.log("Sorry, there's no option for that!")
      rl.close()
    }
  });
  