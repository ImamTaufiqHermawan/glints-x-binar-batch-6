# Session 07

### Readline module

```javascript
const readline = require('readline');
```

The Readline module provides a way of reading a datastream, one line at a time.

```js
const rl = readline.creatInterface (
    process.stdin,
    process.stdout
    );
```

process.stdin means "input coming from the terminal"

process.stdout means for output, use the standard output stream (i.e. terminal console output)

We can say these code above to get input from a readable stream such as the process.stdin stream, which during the execution of a Node.js program is the terminal input, one line at a time.

function how to get an answer from user and process it to the result with readline

 ```js
 function getAreaInput() {
    rl.question("Side: ", answer => {//answer is a parameter 
        console.log("Here is the result", square.area(+answer)// +(plus) is for converting string answer to number
        )
        rl.close()// to close the readline after answering the question
    })
}
```

handle answer with if function

```js
function handleAnswer(answer) {
    if (answer == 1) getAreaInput();
    if (answer == 2) getRoundInput();
}

rl.question("Answer: ", answer => {
    handleAnswer(answer);
})
```

### SwitchCase

If we need to check only 1 variable, we can simply use this instead of if-else statement

```js
let answer = 1
switch(answer) {
    case 1:
         console.log('it is an odd number');)
         break;
    case 2: 
        console.log("it is an even number")
        break;
    default:
    console.log("not available")

// Or use return
switch (answer) {
  case 1:
    return console.log("Hello from switch");

  case 2:
    return console.log("Jawaban dua");

  case 3:
    return console.log("Jawaban tiga");

  default:
    // Option is not available
    return console.log("Jawaban gaada");
}

```

### Import and Export module
We make square.js so we can export it to index.js. Below is square.js :

```js
function area(s) {
  return s * s;
}

function round(s) {
  return 4 * s;
}

// Export function that will be used in another file
module.exports = {
  area,
  round
};
```

Below this is index.js :

``` js
// Import Readline Module
// Node.JS, Common JS
const readline = require("readline");

//import function from square.js that will be use in this file
const square = require("./square.js"); 

// Create interface
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// To ask user an input that will be used on area function
function getAreaInput() {
  rl.question("Side: ", answer => {
    console.log(
      "Here's the result",
      square.area(+answer)
    )

    rl.close()
  })
}

// To ask user an input that will be used on round function
function getRoundInput() {
  rl.question("Side: ", answer => {
    console.log(
      "Here's the result",
      square.round(+answer)
    )

    rl.close()
  }) 
}

console.clear(); // To clear up the console
console.log(`Which operation do you want to do?
1. Calculate Square Area
2. Calculate Square Round
`)

function handleAnswer(answer) {
  console.clear();
  switch (+answer) {
    case 1: return getAreaInput();
    case 2: return getRoundInput();
    default:
      console.log("Option is not available");
      rl.close();
  }
}

rl.question("Answer: ", answer => {
  handleAnswer(answer);
});

rl.on("close", () => {
  process.exit;
})
```

### Regular Expression 
Usually called `Regex`is an operation to validate that the data strict to contain something

```js
//the $ symbol is for stricting the data
let numberOnly = /^[0-9]+$/ 
numberOnly.test("12345")
// the answer is True or false
```

### JSON Format

JSON file for store data, like data that user input will be store in .json file with this code :

```js
function register(email, password) {
  fs.writeFileSync(
    path.resolve(__dirname, '.', 'data.json'), // path module
    JSON.stringify({
      email, password
    }, null, 2) // Change data into string, with JSON format.
  );
}
// __dirname is consistent
```

### Ternary Operator

The ternary operator is an operator that takes three arguments. The first argument is a comparison argument, the second is the result upon a true comparison, and the third is the result upon a false comparison. If it helps you can think of the operator as shortened way of writing an if-else statement.

```js
let number = 10;
for (i = 1; i < number; i++) { i % 2 === 0 ? 'continue' : console.log('The odd numbers: ' + i)};
```