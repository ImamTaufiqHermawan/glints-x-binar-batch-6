const products = require('./products.json');
const users = require('./users.json');

/*
  Code Challenge #3

  Your goals to create these endpoint
    
    /products
      This will show all products on products.json as JSON Response

    /products/available
      This will show the products which its stock more than 0 as JSON Response

    /users
      This will show the users data inside the users.json,
      But don't show the password!

  */

const http = require('http');

/* Code Here */
const PORT = 3000;
const app = http.createServer((req, res) => {
  switch(req.url) {
    case '/':
      res.write("Hello Code Challenge #3\n");
      break;

    case '/products':     
      res.write(JSON.stringify(products));
      break;

    case '/products/available':
      const availableProduct = products.filter((count, i) => {
        if (count.stock > 0) return true;
      });  
      res.write(JSON.stringify(availableProduct));
      break;

    case '/users':
      users['password'] = '*********';
      res.write(JSON.stringify(users));
      break;

    case '/error':
      try {
        throw new Error("This is error!");
      }
      catch(err) {
        res.writeHead(500);
        res.write(err.message);
      }
      break;

    default:
      res.writeHead(404);
      res.write("404 Not Found!\n")
  }

  res.end();
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
