class Human {
  constructor(name, languange) {
    this.name = name;
    this.languange = languange;
  }

  greeting() {
    let currDate = new Date();
    let time = currDate.getHours();
    if (time < 12) {
      console.log(`Good Morning ${PersonB.name}, nice to meet you. I am ${this.name}.`);
    } else if (time < 18) {
      console.log(`Good Afternoon ${PersonB.name}, nice to meet you. I am ${this.name}.`);
    } else {
      console.log(`Good Evening ${PersonB.name}, nice to meet you. I am ${this.name}.`);
    }
  }

  marry() {
    // this code make random Person who learn new Languange. it coulb be Person A or Person B
    let chosenPerson = Math.random() < 0.5 ? this.name : PersonB.name;
    const learnLanguange = chosenPerson === this.name ? PersonB.languange : this.languange;
    const oriLanguange = chosenPerson === this.name ? this.languange : PersonB.languange; 
    const NotChosenPerson = oriLanguange === this.languange ? PersonB.name : this.name;

    if (PersonA.languange === PersonB.languange) {
      console.log(`${this.name} can marry ${PersonB.name} because they understand the same languange`);
    } else {
      console.log(`Ups, ${this.name} and ${PersonB.name} don't have same languange.`);
      console.log(`Then first, ${chosenPerson} will learn ${learnLanguange} so they can get married.`);
    }
    console.log(`After ${chosenPerson} learn new languange ...`);
    console.log(`Hi my name is ${chosenPerson}, my languange is ${oriLanguange}, but i learned ${learnLanguange}, so that I can married to ${NotChosenPerson}.`);
  }

  afterMarried() {
    this.greeting();
    this.marry();
  }
}

const PersonA = new Human("Person A","English");
const PersonB = new Human("Person B","Bahasa");

PersonA.afterMarried();

