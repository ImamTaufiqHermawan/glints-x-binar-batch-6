const fs = require('fs');

// const readline = require('../lib/readline.js');

function Coba (name, address, lang, progammingLangs ) {
  this.name = name;
  this.address = address;
  this.lang = lang;
  this.progammingLangs = progammingLangs;
}

const Ian = new Coba ("Ian", "Malang", 7,"Javascript")

const Imam = new Coba ("Imam", "Bandung", 10, "Java")

const adri = new Coba ("Adri", "Jogja", 2, "PHP")

console.log(Imam.address, Ian.address);

fs.writeFileSync(`${__dirname}/user.json`, JSON.stringify(Ian, null, 2))

const hasFiveOrMore = el => el.count >= 5;

// Requiring users file 
const userss = require("./user.json"); 

console.log(userss); 

let koko = ({
  name: "Harry Potter",
  address: "J.K.Rowling",
  lang: "1.000.000",
  progammingLangs: "Javas"
})

userss.push(koko);

fs.writeFileSync("users.json", JSON.stringify(userss), err => { 
     
  // Checking for errors 
  if (err) throw err;  
 
  console.log("Done writing"); // Success 
}); 

// find with if condition
const evergreens = [
  { name: "cedar", count: 2 },
  { name: "fir", count: 6 },
  { name: "pine", count: 3 }
];



// suppose we need to skip the first element
const result = evergreens.find((tree, i) => {
  if (tree.count > 1 && i !== 0) return true;
});

console.log(result);
// { name: "fir", count: 6 }