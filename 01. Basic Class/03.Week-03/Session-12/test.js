const fs = require('fs');

//const readline = require('./lib/readline.js');

class Record {
  constructor(props) {
    if (this.constructor == Record) 
      throw new Error("Can't instantiate from Record");
    
    this._validate(props);
    this._set(props);
  }

  _validate(props) {
    if (typeof props !== 'object' || Array.isArray(props))
      throw new Error("Props must be an object");
    

    this.constructor.properties.forEach(i => {
      if (!Object.keys(props).includes(i))
        throw new Error(`${this.constructor.name}: ${i} is required`)
    })
  }

  _set(props) {
    this.constructor.properties.forEach(i => {
      this[i] = props[i];
    })
  }
 
  static get all() {
    try {
      return eval(
        fs.readFileSync(`${__dirname}/${this.name}.json`)
          .toString()
      )
    }
    catch {
      return []
    }
  }

  static find(id) {
     let result = this.all.find(i => i.id === id);
     if (!result) return null

     return new this(result);
     result.id = id;

     return result;
  }

  update(id) {

  }

  delete(id) {

  }

  save() {
    fs.writeFileSync(
      `${__dirname}/${this.constructor.name}.json`,
      JSON.stringify([...this.all, { id: this.all.length + 1, ...this } ], null, 2)
    );
  }
}

class book extends Record {

    static properties = [
        "book",
        "author",
        "price"
    ]

    constructor(props) {
        super(props);
    }
}

let Harry = new book({
    book: "Harry Potter",
    author: "J.K.Rowling",
    price: "1.000.000"
})

let GOT = new book({
    book: "Game Of Throne",
    author: "Imam",
    price: "1.000.000.000"
})

class Products extends Record {

    static properties = [
        "product",
        "expire",
        "price"
    ]

    constructor(props) {
        super(props);
    }
}

let soap = new Products({
    product: "Harry Potter",
    expire: "J.K.Rowling",
    price: "1.000.000"
})

let shampoo = new Products({
    product: "Game Of Throne",
    expire: "Imam",
    price: "1.000.000.000"
})

shampoo.save();
GOT.save();