module.exports = {
    circleArea(req, res){
        return res.status(200).json({
            status: true,
            result: 3.14 *(req.body.R*2)
        })
    }
}