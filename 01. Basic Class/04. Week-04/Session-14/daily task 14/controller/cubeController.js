module.exports = {
    cubeVolume(req, res){
        return res.status(200).json({
            status: true,
            result: req.body.S*req.body.S*req.body.S
        })
    }
}