module.exports = {
    tubeVolume(req, res){
        return res.status(200).json({
            status: true,
            result: 3.14 * (req.body.R ** 2) * req.body.H
        })
    }
}