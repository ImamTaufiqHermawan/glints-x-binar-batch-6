const express = require('express');
const router = express.Router();

const {
    info
} = require('./info.js')
const {
    circleArea
} = require('./controller/circleController.js');
const {
    squareArea
} = require('./controller/squareController.js');
const {
    cubeVolume
} = require('./controller/cubeController.js');
const {
    triangleArea
} = require('./controller/triangleController.js');
const {
    tubeVolume
} = require('./controller/tubeController.js');


router.get('/', info);
router.post('/calculate/circleArea', circleArea);
router.post('/calculate/squareArea', squareArea);
router.post('/calculate/cubeVolume', cubeVolume);
router.post('/calculate/triangleArea', triangleArea);
router.post('/calculate/tubeVolume', tubeVolume);

module.exports = router;