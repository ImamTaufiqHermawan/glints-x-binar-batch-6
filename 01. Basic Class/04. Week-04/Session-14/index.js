const express = require('express');
const app = express()

const user = require('./db/user.json');
const product = require('./db/product.json');

/*

    Success Response Code
    200 => OK
    201 => Created
    204 => No Content

    Client Error Response Code
    400 => Bad Request
    401 => Unauthorized
    403 => Forbidden
    404 => Not Found
    405 => Not Allowed
    418 => I'm a teapot
    422 => Unprocessable Entity

    Server Error Response Code // This is totally your fault >:(
    500 => Internal server error
    503 => Bad Gateway

    For more info https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
    
*/

// Middleware to Parse JSON
app.use(express.json());
 
// GET /
app.get('/', function (req, res) {
  res.send('Hello World')
})

// GET /products
app.get('/products', function(req, res) {
  res.json(product);
})

app.get('/products/available', function(req, res) {
  res.json(product.filter(i => i.stock > 0))
})

app.get('/user', (req, res) => {
  let entity = {...user};
  delete entity.password
  res.status(200).json(entity)
})

app.post('/login', (req, res) => {
  if (user.email !== req.body.email)
    return res.status(401).json({
      status: false,
      message: "Email doesn't exist!"
    })

  if (user.password !== req.body.password)
    return res.status(401).json({
      status: false,
      message: "Wrong password!"
    })

  res.status(200).json({
    status: true,
    message: "Succesfully logged in"
  })
})
 
app.listen(3000, () => {
  console.log("Listening on port 3000!");
})  