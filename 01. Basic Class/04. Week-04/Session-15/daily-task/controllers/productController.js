const Product = require('../models').product;

function create(req, res) {
  const product = {
    name: req.body.name,
    price: req.body.price,
    stock: req.body.stock
  }

  Product.create(product)
    .then(data => {
      res.status(200).json({
        status: "success",
        data,
        message: "Success creating the Product"
      });
    })
    .catch(err => {
      res.status(500).send({
        status : "Fail",
        message: "Some error occurred while creating the Product."
      });
    });
};

function all(req, res) {
  Product.findAll().then(data =>
    res.status(200).json({
      status: true,
      data
    })
  )
}

function update(req, res) {
  const id = req.params.id;

  Product.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.status(200).json({
          status : "Success",
          data: {
            id: req.params.id,
            name: req.body.name,
            price: req.body.price,
            stock: req.body.stock
          },
          message: "Product update successfully"
        });
      } else {
        res.status(400).json({
          status : "Fail",
          data: {
            id: req.params.id,
            name: req.body.name,
            price: req.body.price,
            stock: req.body.stock
          },
          message: "Product cannot be update"
        });
      }
    })
    .catch(err => {
      res.status(500).json({
        status : "Fail",
        message: "Error updating Product with id=" + id
      });
    });
};

function remove (req, res) {
  const id = req.params.id;

  Product.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.json({
          status : "Success",
          data: {
            id: req.params.id,
          },
          message: "Product was deleted successfully!"
        });
      } else {
        res.status(400).json({
          status : "Fail",
          message: `Cannot delete Product with id=${id}.`
        });
      }
    })
    .catch(err => {
      res.status(500).json({
        status : "Fail",
        message: "Could not delete Product with id=" + id
      });
    });
};

module.exports = {
  create, all, update, remove
}