const express = require('express');
const router = express.Router();
const product = require('./controllers/productController');

/* Product API Collection */
router.post('/create', product.create);
router.get('/products', product.all);
router.put('/update/:id', product.update);
router.delete('/remove/:id', product.remove);

module.exports = router;