const { user: User } = require('../models');
const bcrypt = require('bcryptjs');

module.exports = {

  register(req, res) {
    /*
      1. Data from User
      2. Change plain password into encrypted password using bcrypt
      3. Then save the result to the users table.
    */
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(req.body.password, salt);

    User.create({
      email: req.body.email.toLowerCase(),
      encrypted_password: hash
    })
      .then(user => {
        res.status(200).json({
          status: "success",
          data: {
            user
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: "fail",
          errors: [err.message]
        })
      })
  },

  login (req, res) {
    User.findOne({
      where: {
        email: req.body.email
      }
    })
    .then(user => {
      if(!user) {
        res.send('User not found');
      }
      const result = bcrypt.compareSync(req.body.password, user.encrypted_password);
      if(!result){
        res.status(500).json({
           message: 'Unable to login'
        })
      }else{
        res.status(200).json({
          message: 'Succes to login'
        })
      }
    })
    .catch(() => res.status(500).json({
      status: 'error',
      messsage: 'Unable to communicate with database' 
    }))
  }
}