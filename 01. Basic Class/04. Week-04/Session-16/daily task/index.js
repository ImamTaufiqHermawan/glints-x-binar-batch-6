const express = require('express');
const app = express();
const router = require('./router.js');

// Use middleware
app.use(express.json());

app.get('/', (req, res) => {
  res.status(200).json({
    status: "success",
    message: "Hello World"
  })
})

// Use the router middleware
app.use('/', router);

app.listen(3000, () => {
  console.log("Listening on port 3000!");
})