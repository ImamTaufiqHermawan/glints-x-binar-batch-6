const router = require('express').Router();
const productController = require('./controllers/productController');
const userController = require('./controllers/userController');

/* Product API Collection */
router.post('/products', productController.create);
router.get('/products', productController.show);
router.get('/products/available', productController.available);
router.get('/products/:id', productController.findById);
router.put('/products/:id', productController.update);

router.post('/users/register', userController.register);
router.post('/users/login', userController.login);

module.exports = router;