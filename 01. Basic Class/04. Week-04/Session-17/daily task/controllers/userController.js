const { user: User } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {

  register(req, res) {
    User.create({
      email: req.body.email.toLowerCase(),
      encrypted_password: req.body.password
    })
      .then(user => {
        res.status(201).json({
          status: 'success',
          data: {
            user
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: "fail",
          errors: [err.message]
        })
      })
  },

  login(req, res) {
    User.findOne({
      where: { email: req.body.email.toLowerCase() }
    })
      .then(instance => {
        if (!instance)
          return res.status(401).json({
            status: 'fail',
            errors: ["Email doesn't exist"]
          })

        let isPasswordValid = bcrypt.compareSync(req.body.password, instance.encrypted_password);

        if (!isPasswordValid)
          return res.status(401).json({
            status: 'fail',
            errors: ["Wrong password!"]
          })

        const token = jwt.sign({
          id: instance.id,
          email: instance.email
        }, 'Rahasia')

        res.status(201).json({
          status: 'success',
          data: {
            token
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        })
      })
  },

  me (req, res) {
    let token = req.headers.authorization;
    let payload;

    try {
      payload = jwt.verify(token, 'Rahasia');
    }
    catch(err) {
      return res.status(401).json({
        status: 'fail',
        errors: [
          "Invalid Token"
        ]
      })
    }

    User.findByPk(payload.id)
    .then(instance => {
      res.status(200).json({
        status: 'succes',
        data: {
          user: instance
        }
      })
    })
  }  
}