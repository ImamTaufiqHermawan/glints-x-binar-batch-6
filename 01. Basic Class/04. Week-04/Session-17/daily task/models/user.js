'use strict';
const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    email: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: true,
        isLowercase: true
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      validate: {
        min: 6
      }
    }
  }, {
    hooks: {
      beforeValidate: instance => {
        instance.email = instance.email.toLowerCase();
      },

      beforeCreate: instance => {
        
        // Change the encrypted_password into hashed encrypted_password
        instance.encrypted_password = bcrypt
          .hashSync(instance.encrypted_password, 10)
      }
    }
  });

  /*

  user.beforeCreate(instance => {       
    // Change the encrypted_password into hashed encrypted_password
    instance.encrypted_password = bcrypt
      .hashSync(instance.encrypted_password, 10)
  })

  */

  user.associate = function(models) {
    // associations can be defined here
  };

  return user;
};
