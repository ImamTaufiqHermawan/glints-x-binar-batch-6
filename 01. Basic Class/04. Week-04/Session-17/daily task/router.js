const router = require('express').Router();
const productController = require('./controllers/productController');
const userController = require('./controllers/userController');
const middleware = require('./middleware');

/* Product API Collection */
router.post('/products', middleware.checkToken, productController.create);
router.get('/products', productController.show);
router.get('/products/available', productController.available);
router.get('/products/:id', productController.findById);
router.put('/products/:id', middleware.checkToken, productController.update);
router.delete('/products/remove/:id', middleware.checkToken, productController.remove);

router.post('/users/register', userController.register);
router.post('/users/login', userController.login);
router.get('/users/me', userController.me);

module.exports = router;
