const { product: Product } = require('../models');
const { Op } = require('sequelize');

module.exports = {

  create(req, res) {
    Product.create(req.body)
      .then(product => {
        res.status(201).json({
          status: 'success',
          data: {
            product
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        })
      });
  },

  show(req, res) {
    Product.findAll()
      .then(products => {
        res.status(201).json({
          status: 'success',
          data: {
            products
          }
        })
      })
  },

  findById(req, res) {
    Product.findByPk(req.params.id)
      .then(product => {
        res.status(201).json({
          status: 'success',
          data: {
            product
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        })
      })
  },

  available(req, res) {
    Product.findAll({
      where: {
        stock: {
          [Op.gt]: 0
        }
      }
    })
      .then(products => {
        res.status(201).json({
          status: 'success',
          data: {
            products
          }
        })
      })
  },

  update(req, res) {
    Product.update(req.body, {
      where: {
        id: req.params.id
      }
    })
      .then(message => {
        res.status(201).json({
          status: 'success',
          payload: message,
          message: `Product with ID ${req.params.id} is succesfully updated!`
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        })
      })
  }
}
