const { user: User } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const successHandler = require('../middlewares/statusHandler')

module.exports = {

  register(req, res, next) {
    User.create({
      email: req.body.email,
      encrypted_password: req.body.password
    })
      .then(user => {
        let data = User.create(req.body);
        successHandler(res, data)
      })
      .catch(err => {
        res.status(422);
        next(err)
      })
  },

  login(req, res, next) {
    User.findOne({
      where: { email: req.body.email.toLowerCase() }
    })
      .then(instance => {
        if (!instance)
         throw new Error("Email doesn't exist")

        let isPasswordValid = bcrypt.compareSync(req.body.password, instance.encrypted_password);

        if (!isPasswordValid)
          throw new Error("Wrong password")

        const token = jwt.sign({
          id: instance.id,
          email: instance.email
        }, 'RAHASIA')

        res.status(201).json({
          status: 'success',
          data: {
            token
          }
        })
      })
      .catch(err => {
        res.status(422);
        next(err)
      })
  },

  // Protected Endpoint, you must have token to make this run
  me(req, res, next) {
    res.status(200).json({
      status: 'success',
      data: {
        user: req.user
      }
    })
  }

}
