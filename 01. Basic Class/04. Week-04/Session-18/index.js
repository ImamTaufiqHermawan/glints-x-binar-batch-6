const express = require('express');
const app = express();
const router = require('./router');
const morgan = require('morgan');
const exception = require('./middlewares/exceptionHandler');

// Use middleware
app.use(morgan('dev'));
app.use(express.json());
app.get('/', (req, res) => {
  res.status(200).json({
    status: "success",
    message: "Hello World"
  })
})
// Use the router middleware
app.use('/', router);

// Error Handler
exception.forEach(handler =>
  app.use(handler)
);

app.listen(3000, () => {
  console.log("Listening on port 3000!");
})