const jwt = require('jsonwebtoken');
const { user: User } = require('../models');

module.exports = async (req, res, next) => {
  try {
    let token = req.headers.authorization; 
    let payload = await jwt.verify(token, 'RAHASIA');
    
    // Find the user instance and assign to the req.user
    let user = await User.findByPk(payload.id)
    req.user = user;
    next();
  }

  catch {
    res.status(401);
    next(new Error("Invalid Token"));
  }
}