const Post = require ('../models').post;
const User = require ('../models').users;
const success = require('../middlewares/succesHandler');
const jwt = require('jsonwebtoken');
const {Op} = require('sequelize')

module.exports = {
  async createPost(req, res,next) {
      try{
          const post = await Post.create({
              title: req.body.title,
              body: req.body.body,
              userId: req.user.id
          });
          success(res, 201, post, 'Post');
      }
      catch(err) {
          res.status(422);
          next(err);
      }
  },

  async allPost (req, res, next) {
      try {
              let posts = await Post.findAll({ 
                  include: [
                      {
                          model: User,
                          attributes: ['email','role']
                      }
                  ] 
              });
              let isAdmin = req.user.id;
              if(isAdmin.role != 'admin'){
                  posts = posts.filter(post => post.approved == true)
              }
              success(res, 200, posts, 'Posts');
            
      }
      catch(err) {
          res.status(422);
          next(err);
      }
  },

  async updatePost(req, res, next)  {
      try {
          if(Post.userId == req.user.id) {
              const { title, body } = req.body
              const post = await Post.update({ title, body }, {
                  where: { id: req.params.id }
              })
              res.status(201).json({
                  status: 'Success',
                  message: `Successfuly updated post with Id: ${postId}`,
                  result: post
              })
          }else{
              res.status(404).json({
                  status: 'Fail',
                  message: 'Not Found'
              })
          }   
      } 
      catch (err) {
          res.status(422);
          next(err);
      }
  },

  async searchPost (req, res ) {
      try {

          let result = await Post.findAll({
              where: {
                  [Op.or]: {
                      title: {
                          [Op.like]: `%${req.query.title}%`
                      },
                      body: {
                          [Op.like]: `%${req.query.body}%`
                      }
                  }

              },
              order: [
                  ['createdAt', 'Asc']
              ]
          })
          console.log(req.query);
          res.status(201).json({
              status: 'Success',
              data: result
          })

      } 
      catch(err) {
          res.status(422);
          // next(err);
      }
  },

  async deletePost (req, res, next) {
      try {
          let isAdmin = await User.findByPk(req.user.id);
          if(isAdmin.role == 'admin') {
              const post = await Post.destroy({ where: { id: req.params.id } })
              res.status(200).json({
                  status: 'Success',
                  message: `Successfully deleted post with Id: ${req.params.id}`,
                  post
              })
          }else{
              res.status(403);
          }
      } 
      catch (err) {
          res.status(422);
          next(err);
      }
  },

  async approvePost(req, res, next) {
      try{
          let isAdmin = await User.findByPk(req.user.id);
          if(isAdmin.role == 'admin') {
              let post = await Post.update({ approved: true }, {
                      where: {
                          id: req.params.id
                      }
              });
              post =  await Post.findByPk(req.params.id);        
              success(res, 200, posts, 'Posts');

          }
          else{
              res.status(404).json({
                  status: 'Fail',
                  message: 'You can not approve'
              })
          }
      }
      catch(err) {
          res.status(422);
          next(err);
      }
  }
}