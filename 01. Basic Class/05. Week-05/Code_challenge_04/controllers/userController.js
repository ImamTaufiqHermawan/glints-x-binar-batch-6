const { users } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require ('jsonwebtoken');

module.exports = {
    async register(req, res, next) {
        try {
            const hashedPassword = await bcrypt.hash(req.body.password, 10);
            let user = await users.create({
                email: req.body.email.toLowerCase(), 
                encrypted_password: hashedPassword,
                role: req.body.role})
                res.status(201).json({
                    "status": "success",
                    data: {user}
                })
        }
        catch (err) {
            res.status(400).json({
              "status": "failed",
              "message": [err.message]
          })
        }
    },
    async login(req, res, next) {
        let user = await users.findOne({
            where:{
                email:req.body.email 
            }
        })
        if (user == null) {
            return res.status(400).json({
                "status": "failed",
                "message": "user doesn't exist or email must be fill with lowercase"
            })
        }
        const token = await jwt.sign({
            id: users.id,
            email: users.email
          }, 'Rahasia')
        try {
            if(await bcrypt.compare(req.body.password, user.encrypted_password)) {
                res.status(201).json ({
                    "status": "Success",
                    data: {token}
                }) 
            } else {
                res.status(400).json ({
                    "status": "failed",
                    "message": "wrong password"
                })
            }
        } 
        catch (err) {
            res.status(500).json({
              "status": "failed",
              "message": [err.message]
          })
        }
    },
    //this is only to try if token authenticate
    async show(req, res, next) {
        try{
            let name = await profile.findAll();
            res.status(200).json ({
                "status": "success",
                data : {name}
            })
        }
        catch (err) {
            res.status(422).json ({
                "status": "failed",
                "message": [err.message]
            })
        }
    }    
}