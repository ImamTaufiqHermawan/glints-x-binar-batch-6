const express = require('express');
const app = express();
const router = require('./router.js');
const morgan = require("morgan");

// Use middleware
app.use(express.json());
app.use(morgan("tiny"));

app.get('/', (req, res) => {
  res.status(200).json({
    status: "success",
    message: "Hello World"
  })
})

// Use the router middleware
app.use("/api/v1", router);

app.listen(3000, () => {
  console.log("Listening on port 3000!");
})