let jwt = require('jsonwebtoken');
const {users} = require ('../models');

module.exports = {
    checkToken (req, res, next) {
        let token = req.headers.authorization;
        if (token) {
            jwt.verify(token, "Rahasia", (err, decoded) => {
              if (err) {
                return res.json({
                  status: false,
                  message: 'Token is not valid'
                });
              } else {
                req.decoded = decoded;
                next();
              }
            });
        } else {
            return res.json({
              status: false,
              message: 'Auth token is not supplied'
            });
        }
    }
}