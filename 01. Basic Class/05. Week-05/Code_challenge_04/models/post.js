'use strict';
module.exports = (sequelize, DataTypes) => {
  const post = sequelize.define('post', {
    title: DataTypes.STRING,
    body: DataTypes.STRING,
    approve: DataTypes.BOOLEAN,
    userID: DataTypes.INTEGER
  }, {});
  post.associate = function(models) {
    // associations can be defined here
    post.belongsTo(models.user, {
      foreignKey: 'userId'
    })
  };
  return post;
};