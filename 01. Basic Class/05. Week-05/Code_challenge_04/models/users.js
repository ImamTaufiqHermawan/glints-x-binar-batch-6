'use strict';
const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    email: { 
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: {
          msg: 'can only be filled in by email'
        },
        isLowercase: true
      }
    },
    encrypted_password: DataTypes.STRING,
    role: DataTypes.STRING
  }, {});
  users.associate = function(models) {
    user.hasMany(models.post, {
      foreignkey: 'userId'
    })
    // associations can be defined here
  };
  return users;
};