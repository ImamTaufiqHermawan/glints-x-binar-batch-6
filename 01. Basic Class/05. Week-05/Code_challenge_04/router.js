const router = require('express').Router();
const userController = require('./controllers/userController');
const postController = require('./controllers/postController');
const authenticate = require('./middlewares/authenticate');
const checkOwnership = require('./middlewares/checkOwnership');

router.post('/users/register', userController.register);
router.post('/users/login', userController.login);
router.get('/users/me', userController.me);

router.get('/post/',authenticate, postController.allPost);
router.get('/post/search',authenticate, postController.searchPost);
router.post('/post/',authenticate,postController.createPost);
router.put('/post/:id',authenticate,checkOwnership('post'),postController.updatePost);
router.delete('/post/:id',authenticate,checkOwnership('post'),postController.deletePost);
router.put('/post/approve/:id',authenticate,postController.approvePost);

module.exports = router;