const Product = require('../models').product;
const Op = require('sequelize');
const success = require('../middlewares/successHandler');

module.exports = {
    create(req,res) {
        Product.create(req.body)
            .then(product => success(res, 201, product))
            .catch(err => {
                res.status(422);
                next(err)
            })
    },
    findAll(req, res) {
        Product.findAll()
        .then(products => success(res, 201, products))
        .catch(err => {
            res.status(422);
            next(err)
        })
    },
    findById(req, res) {
        Product.findByPk(req.params.id)
        .then(product => success(res,  201, product))
        .catch(err => {
            res.status(422);
            next(err)
        })
    },
    available(req, res) {
        Product.findAll({
            where: {
                stock: {
                    [Op.gt]: 0 //GT greater than
                }
            }
        })
        .then(products => success(res, 201, products))
        .catch(err => {
            res.status(422);
            next(errr)
        })
    },
    update(req, res) {
        Product.update(req.body, {
            where: {
                id: req.params.id
            }
        })
        .then( data => Product(res, 201, `Id ${req.params.id} succesfully updated`))
        .catch(err => {
            res.status(422);
            next(errr)
        })
    },
    delete(req, res){
        product.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(data => Product( res, 201, `Id ${req.params.id} successfully deleted`))
        .catch(err => {
            res.status(422);
            next(errr)
        })
    }

}

