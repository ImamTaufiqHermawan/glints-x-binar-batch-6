const express = require('express');
const app = express();
const router = require('./router');
const morgan = require('morgan');
const exception = require('./middlewares/exceptionHandler')

app.use(morgan('dev'));// pake tiny lebis simple dari combined, dev juga simple dan ada warna
app.use(express.json());


app.get('/',(req,res) => {
    res.status(200).json({
        status: "Success",
        message: "hello world"
    })
})
app.use(express.urlencoded({extended: true}));

//use the router middleware
app.use('/api/v1',router);


exception.forEach(handler =>
    app.use(handler)
  );
  

app.listen(3000,() => {
    console.log("Listening on port 3000")
})