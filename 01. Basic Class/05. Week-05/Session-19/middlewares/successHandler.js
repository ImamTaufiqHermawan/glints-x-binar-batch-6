module.exports =(res, code, instance, key) => {
    let result = res.status(code).json({
                 status: "Success",
                 data: {
                     [key]: instance
                    }
                })
    return result
};
