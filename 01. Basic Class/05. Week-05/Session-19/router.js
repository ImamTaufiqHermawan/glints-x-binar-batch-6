const router = require('express').Router();
const productController = require('./controller/productController');
const userController = require('./controller/userController');
const authenticate = require('./middlewares/authenticate');
const postController = require('./controller/postController');


//Product
router.post('/products',authenticate,productController.create);
router.get('/products',productController.findAll);
router.get('/products/available',productController.available);// harus diatas yang pakeid id
router.get('/products/:id',authenticate,productController.findById);
router.put('/products/:id',authenticate,productController.update);
router.delete('/products/:id',authenticate,productController.delete);



//User
router.get('/user',userController.Read);
router.get('/user/me',userController.me);
router.post('/user/register',userController.register);
router.post('/user/login',userController.login);

//post
router.post('/post/create',authenticate,postController.create);
router.get('/post/all',authenticate,postController.all);


module.exports = router