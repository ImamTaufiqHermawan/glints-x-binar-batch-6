'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });
    
    main();    
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the reverseString function
 * Use console.log() to print to stdout.
 */
function reverseString(s) {
    try {
        // split text
        let splitStr = s.split('');
        // Reverse the order of the elements in an array using reverse method
        let reverseArr = splitStr.reverse();
        // Convert the elements of an array into a string using join method
        let joinArr = reverseArr.join('');
        console.log(joinArr);
    }
    catch (error)
        {
            console.log(error.message);
            console.log(s);
        }
}