Const User = require('../models/user')

module.exports = {
    register = (req, res) => {
        // Validate request
        if (!req.body.title) {
          res.status(400).send({ message: "Content can not be empty!" });
          return;
        }
      
        // Create a Tutorial
        const tutorial = new Tutorial({
          title: req.body.title,
          description: req.body.description,
          published: req.body.published ? req.body.published : false
        });
      
        // Save Tutorial in the database
        tutorial
          .save(tutorial)
          .then(data => {
            res.send(data);
          })
          .catch(err => {
            res.status(500).send({
              message:
                err.message || "Some error occurred while creating the Tutorial."
            });
          });
      };
    register(req, res) { 
        const tutorial = new Tutorial({
            title: req.body.title,
            description: req.body.description,
            published: req.body.published ? req.body.published : false
          });
        const hashedPassword = await bcrypt.hash(req.body.password, 10);
        let newUser = await User.create({
            email: req.body.email.toLowerCase(), 
            encrypted_password: hashedPassword
        })
        let userProfile = await Profile.create({
            name: req.body.name, 
            user_id: newUser.id
        })
        const token = await jwt.sign({
            id: newUser.id,
            email: newUser.email
          }, process.env.SECRET_KEY)
            res.status(201).json({
                "status": "success",
                data: {newUser, userProfile, token}
            })
    } else {
        throw new Error('Email already registered');
    } 
} catch (err) {
    console.log(err)
    res.status(400).json({
      "status": "failed",
      "message": [err.message]
    })
} 
},
}
