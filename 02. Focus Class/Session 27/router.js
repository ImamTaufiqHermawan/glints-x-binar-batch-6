const router = require('express').Router();

// Controllers
const index = require('./controllers/indexController')
const post = require('./controllers/postController')
const user = require('./controllers/userController')

router.get('/', index.home)
router.get('/posts', post.all)
router.post('/posts', post.create)


module.exports = router;