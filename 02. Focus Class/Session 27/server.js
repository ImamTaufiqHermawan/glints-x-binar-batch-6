const express = require('express')
const morgan = require('morgan')
const app = express()

require('./confiq/database');
const router = require('./router')

app.use(morgan('dev'))
app.use(express.json())
app.use(router)

module.exports = app;