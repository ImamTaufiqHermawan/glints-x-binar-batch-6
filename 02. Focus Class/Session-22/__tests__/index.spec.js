const request = require('supertest');
const app = require('../index')

// What is test name
// Jest
describe('Root Path', () => {
    // Jest
    describe('GET /', () => {
        // Jest
        test('Should return 200', done => {
            // ini dari supertest
            request(app).get('/')
            // ini juga
            .then(res => {
                // Jest
                expect(res.statusCode).toEqual(200);
                expect(res.body.status).toEqual('success');
                expect(res.body.message).toEqual('Hello World !!!');
                // Done is required
                done();
            })

        })
    })
})