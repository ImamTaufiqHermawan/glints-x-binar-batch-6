const router = require('express').Router();
const index = require ('./controller/indexController')
const post = require('./controller/postController')

router.get('/', index.home)

router.get('/post', post.index);
router.post('/post', post.create)
router.put('/post/:id', post.update)
router.delete('/post/:id', post.delete)

module.exports = router;