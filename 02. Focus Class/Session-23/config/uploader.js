const multer = require('multer');

module.exports = {
  development: {
    storage: multer.diskStorage({
      destination: function (req, file, cb) {
        cb(null, "uploads")
      },
      filename: function (req, file, cb) {
        cb(null, 'IMG' + '-' + Date.now() + '.' + file.mimetype.split('/')[1])
      }
    })
  },
}