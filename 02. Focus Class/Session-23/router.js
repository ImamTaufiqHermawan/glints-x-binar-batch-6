const router = require('express').Router();

// Import the controller
const index = require('./controllers/indexController');
const post = require('./controllers/postController');
const test = require('./controllers/testController');

// Import middleware
const uploader = require('./middlewares/uploader');

// Root Endpoint
router.get('/', index.index);

// Post API Collection
router.post('/posts', post.create);
router.get('/posts', post.index);
router.delete('/posts/:id', post.delete);

// Test API Collection
router.post('/test', uploader.single('image'), test.index);

module.exports = router;
