const path = require('path');
const multer = require('multer');

module.exports = {
  development: {
    /* If you want to use the diskStorage
      storage: multer.diskStorage({
        destination: function (req, file, cb) {
          cb(
            null,
            path.resolve(__dirname, '..', 'public', 'uploads')
          )
        },
        filename: function (req, file, cb) {
          const split = file.originalname.split('.');
          const ext = split[split.length - 1];
          const name = `${file.fieldname.toUpperCase()}-${Date.now()}.${ext}`;
          file.url = `${process.env.BASE_URL}/uploads/${name}`;

          cb(null, name)
        }
      })
    */
  },
  test: {
    /*
      storage: multer.diskStorage({
        destination: function (req, file, cb) {
          cb(
            null,
            path.resolve(__dirname, '..', 'public', 'uploads')
          )
        },
        filename: function (req, file, cb) {
          const split = file.originalname.split('.');
          const ext = split[split.length - 1];
          const name = `TEST-${file.fieldname.toUpperCase()}-${Date.now()}.${ext}`;
          file.url = `${process.env.BASE_URL}/uploads/${name}`;

          cb(null, name)
        }
      })
    */
  },
  production: {},
}
