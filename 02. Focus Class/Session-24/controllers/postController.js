const imagekit = require('../lib/imagekit');
const { Post } = require('../models');

module.exports = {
  index(req, res) {
    Post.findAll()
      .then(posts => {
        res.status(200).json({
          status: 'success',
          data: {
            posts
          }
        })
      }) 
  },

  create(req, res) {
    Post.create(req.body)
      .then(post => {
        res.status(201).json({
          status: 'success',
          data: {
            post
          }
        }) 
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        }); 
      })
  },

  upload(req, res) {
    const split = req.file.originalname.split('.');
    const ext = split[split.length - 1];

    imagekit.upload({
      file: req.file.buffer,
      fileName: `IMG-${Date.now()}.${ext}`
    })
      .then(image => {
        return Post.update({ image: image.url }, {
          where: { id: req.params.id }
        }) 
      })
      .then(() => {
        res.status(200).json({
          status: 'success',
          data: {
            message: "Updated!"
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'failed',
          errors: [err.message]
        })
      })
  },

  delete(req, res) {
    Post.destroy({
      where: { id: req.params.id }
    })
      .then(data => {
        res.status(204).json({
          status: 'success',
          data: {
            message: data
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        })
      })
  },
}
