const imagekit = require('../lib/imagekit');

module.exports = {

  index(req, res) {
    res.send(req.file.url);
  },

  uploadToCDN(req, res) {
		const split = req.file.originalname.split('.');
		const ext = split[split.length - 1];

		imagekit.upload({
				file: req.file.buffer, // required
				fileName: `IMG-${Date.now()}.${ext}`,   // required
		}).then(response => {
			console.log(response);

			res.end()
		}).catch(error => {
			console.log('Error')

			res.end()
		});
  }
}
