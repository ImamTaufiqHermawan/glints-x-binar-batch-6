'use strict';

module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define('Post', {
    title: DataTypes.STRING,
    body: DataTypes.TEXT,
    image: {
      type: DataTypes.TEXT,
      validate: {
        isUrl: true
      }
    }
  }, {});

  Post.associate = function(models) {
    // associations can be defined here
  };

  return Post;
};
