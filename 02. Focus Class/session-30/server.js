require('dotenv').config();
const express = require('express');
const app = express();
const morgan = require('morgan');
const cors = require('cors');
const PORT = process.env.PORT || 3000;

const router = require('./router');

app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
    res.status(200).json({
        "status": "success",
        "message": "HELLO Sesi 30"
    })
})

app.use('/api/v1/', router);

app.listen(PORT,() => {
    console.log(`Listening on port ${PORT}`);
})